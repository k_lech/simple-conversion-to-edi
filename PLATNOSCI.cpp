#include "stdafx.h"
#include "PLATNOSCI.h"
#include <algorithm>

PLATNOSCI::PLATNOSCI()
{
}

void PLATNOSCI::setSposob_platnosci(int sposob_platnosci)
{
	this->sposob_platnosci = sposob_platnosci;
}
void PLATNOSCI::setWartosc_2(string wartosc_2)
{
	this->wartosc_2 = wartosc_2;
}
void PLATNOSCI::setKod_pay(string kod_pay)
{
	this->kod_pay = kod_pay;
}
void PLATNOSCI::setKwota(int kwota)
{
	this->kwota = kwota;
}
void PLATNOSCI::setWaluta(string waluta)
{
	this->waluta = waluta;
}
void PLATNOSCI::getWartosc_2()
{
	cout << "Podaj sposb platnosci \n1.Gotowka / 2.Karta kredytowa / 3.Przelew krajowy / 4. Przelew zagrniczny \n(Cyfra lub slowo)" << endl;
	cin.ignore();
	while (1)
	{
		getline(cin, wartosc_2);
		wartosc_2.erase(remove_if(wartosc_2.begin(), wartosc_2.end(), isspace), wartosc_2.end());
		transform(wartosc_2.begin(), wartosc_2.end(), wartosc_2.begin(), ::tolower);
		if (wartosc_2 == "gotowka" || wartosc_2 == "1")
		{
			sposob_platnosci = 10;
			break;
		}
		else if (wartosc_2 == "kartakredytowa" || wartosc_2 == "2")
		{
			sposob_platnosci = 11;
			break;
		}
		else if ( wartosc_2 == "przelewkrajowy" || wartosc_2 == "3")
		{
			sposob_platnosci = 12;
			break;
		}
		else if (wartosc_2 == "przelewzagrniczny" || wartosc_2 == "4")
		{
			sposob_platnosci = 13;
			break;
		}

		else if (wartosc_2 != "gotowka" || wartosc_2 != "1" || wartosc_2 != "kartakredytowa" || wartosc_2 != "2" || 
			wartosc_2 != "przelewkrajowy" || wartosc_2 != "3" || wartosc_2 != "przelewzagrniczny" || wartosc_2 != "4")
		{
			cout << "Podano nieprawidlowa wersje dokumentu!\n";
			std::cin.clear();
			std::cin.sync();
		}
	}
}
void PLATNOSCI::getKwota()
{
	cout << "Wprowadz kwote transakcji: " << endl;
	while (1)
	{
		cin >> kwota;
		if (cin.good()) break;
		if (cin.fail())
			{
			cout << "Wprowadzona kwota nie jest liczba! Wprowadz jeszcze raz:\n";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
			}
		}
}
void PLATNOSCI::getWaluta()
{
	cout << "Wprowadz walute w ktorej dokonano transakcji \n (Euro / Zloty / GBP / CZK / CHF / RUB)" << endl;
	cin.ignore();
	while (1)
	{
		getline(cin, waluta);
		waluta.erase(remove_if(waluta.begin(), waluta.end(), isspace), waluta.end());
		transform(waluta.begin(), waluta.end(), waluta.begin(), ::tolower);
		if (waluta == "euro" || waluta == "eur")
		{
			waluta = "EUR";
			break;
		}
		else if (waluta == "zloty" || waluta == "zl")
		{
			waluta = "ZL";
			break;
		}
		else if (waluta == "funtbrytyjski" || waluta == "gbp")
		{
			waluta = "GBP";
			break;
		}
		else if (waluta == "koronyczeskie" || waluta == "czk")
		{
			waluta = "CZK";
			break;
		}
		else if (waluta == "koronyczeskie" || waluta == "czk")
		{
			waluta = "CZK";
			break;
		}
		else if (waluta == "frankszwajcarski" || waluta == "chf")
		{
			waluta = "CHF";
			break;
		}
		else if (waluta == "rubel rosyjski" || waluta == "rub")
		{
			waluta = "RUB";
			break;
		}
		else
		{
			cout << "Blad, wprowadz nazwe waluty, lub skrot z legendy. Wprowadz jeszcze raz:\n";
			std::cin.clear();
			std::cin.sync();
		}
	}

}
string PLATNOSCI::getKod_pay()
{
	std::string pay = std::to_string(sposob_platnosci);
	std::string kwota_wyswietl = std::to_string(kwota);	// konwersja z int do string - kwota -> kwota_wy�wietl
	string PAI = "PAI:";
	string UNT = "UNT+";
	kod_pay = PAI + pay + '+' + kwota_wyswietl + '+' + waluta + "'" + '\n' + UNT;
	cout << kod_pay;
	return kod_pay;
}