#include "stdafx.h"
#include <cctype>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include "NABYWCA_DOSTAWCA.h"

NABYWCA_DOSTAWCA::NABYWCA_DOSTAWCA()
{
}

void NABYWCA_DOSTAWCA::setNabywca(string nabywca)
{
	this->nabywca = nabywca;
}
void NABYWCA_DOSTAWCA::setOdbiorca(string odbiorca)
{
	this->odbiorca = odbiorca;
}
void NABYWCA_DOSTAWCA::setKod_pocztowy_n(string kod_pocztowy_n)
{
	this->kod_pocztowy_n = kod_pocztowy_n;
}
void NABYWCA_DOSTAWCA::setKod_pocztowy_o(string kod_pocztowy_o)
{
	this->kod_pocztowy_o = kod_pocztowy_o;
}
void NABYWCA_DOSTAWCA::setNIP_Nabywca(long long nip_n)
{
	this->nip_n = nip_n;
}
void NABYWCA_DOSTAWCA::setNIP_Odbiorca(long long nip_o)
{
	this->nip_o = nip_o;
}
void NABYWCA_DOSTAWCA::setMiasto_n(string miasto_n)
{
	this->miasto_n = miasto_n;
}
void NABYWCA_DOSTAWCA::setMiasto_o(string miasto_o)
{
	this->miasto_o = miasto_o;
}
void NABYWCA_DOSTAWCA::setKod_AB(string kod_AB)
{
	this->kod_AB = kod_AB;
}
void NABYWCA_DOSTAWCA::setKod_BB(string kod_BB)
{
	this->kod_BB = kod_BB;
}


void NABYWCA_DOSTAWCA::getNabywca()
{
	cout << "Wprwoadz nazwe firmy nabywajacej: " << endl;
	cin.ignore();
	getline(cin, nabywca);
}
void NABYWCA_DOSTAWCA::getOdbiorca()
{
	cout << "Wprwoadz nazwe firmy odbierajacej: " << endl;
	cin.ignore();
	getline(cin, odbiorca);
}
void NABYWCA_DOSTAWCA::getKod_pocztowy_n()
{
	cout << "Wprowadz kod pocztowy miasta - nabywcy, w formacie xx-xxx: " << endl;
	
	while (1) 
	{
		cin >> kod_pocztowy_n;
		if (kod_pocztowy_n.length() > 6)
		{
			cout << "Blad - podano dluzszy kod, niz wymagano. Kod pocztowy powinien miec format: 00-000" << endl;
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_n.at(2) != '-')
		{
			cout << "Blad - podano niepoprawny format kodu. Kod pocztowy powinien miec format: 00-000" << endl;
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_n.at(0) >= 'a' && kod_pocztowy_n.at(0) <= 'z' || kod_pocztowy_n.at(0) >= 'A' && kod_pocztowy_n.at(0) <= 'Z')
		{
			cout << "Blad nie podano liczby1! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_n.at(1) >= 'a' && kod_pocztowy_n.at(1) <= 'z' || kod_pocztowy_n.at(1) >= 'A' && kod_pocztowy_n.at(1) <= 'Z')
		{
			cout << "Blad nie podano liczby2! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_n.at(3) >= 'a' && kod_pocztowy_n.at(3) <= 'z' || kod_pocztowy_n.at(3) >= 'A' && kod_pocztowy_n.at(3) <= 'Z')
		{
			cout << "Blad nie podano liczby3! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_n.at(4) >= 'a' && kod_pocztowy_n.at(4) <= 'z' || kod_pocztowy_n.at(4) >= 'A' && kod_pocztowy_n.at(4) <= 'Z')
		{
			cout << "Blad nie podano liczby4! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_n.at(5) >= 'a' && kod_pocztowy_n.at(5) <= 'z' || kod_pocztowy_n.at(5) >= 'A' && kod_pocztowy_n.at(5) <= 'Z')
		{
			cout << "Blad nie podano liczby5! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else break;
	}
}
void NABYWCA_DOSTAWCA::getKod_pocztowy_o()
{
	cout << "Wprowadz kod pocztowy miasta - odbiorcy, w formacie xx-xxx: " << endl;

	while (1)
	{
		cin >> kod_pocztowy_o;
		if (kod_pocztowy_o.length() > 6)
		{
			cout << "Blad - podano dluzszy kod, niz wymagano. Kod pocztowy powinien miec format: 00-000" << endl;
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_o.at(2) != '-')
		{
			cout << "Blad - podano niepoprawny format kodu. Kod pocztowy powinien miec format: 00-000" << endl;
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_o.at(0) >= 'a' && kod_pocztowy_o.at(0) <= 'z' || kod_pocztowy_o.at(0) >= 'A' && kod_pocztowy_o.at(0) <= 'Z')
		{
			cout << "Blad nie podano liczby1! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_o.at(1) >= 'a' && kod_pocztowy_o.at(1) <= 'z' || kod_pocztowy_o.at(1) >= 'A' && kod_pocztowy_o.at(1) <= 'Z')
		{
			cout << "Blad nie podano liczby2! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_o.at(3) >= 'a' && kod_pocztowy_o.at(3) <= 'z' || kod_pocztowy_o.at(3) >= 'A' && kod_pocztowy_o.at(3) <= 'Z')
		{
			cout << "Blad nie podano liczby3! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_o.at(4) >= 'a' && kod_pocztowy_o.at(4) <= 'z' || kod_pocztowy_o.at(4) >= 'A' && kod_pocztowy_o.at(4) <= 'Z')
		{
			cout << "Blad nie podano liczby4! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else if (kod_pocztowy_o.at(5) >= 'a' && kod_pocztowy_o.at(5) <= 'z' || kod_pocztowy_o.at(5) >= 'A' && kod_pocztowy_o.at(5) <= 'Z')
		{
			cout << "Blad nie podano liczby5! Podaj kod pocztyowy jeszcze raz.";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
		else break;
	}
}
void NABYWCA_DOSTAWCA::getNIP_Nabywca()
{
	cout << "Podaj nr NIP firmy nabywajacej (10 cyfr)" << endl;
	while (1)
	{
		cin >> nip_n;
		if (cin.fail())
		{
			cout << "Podany Nr NIP nie jest liczba! Prosze wprowadzic Nr NIP jeszcze raz:\n";
			std::cin.clear();
			std::cin.ignore(10000, '\n');
			continue;
		}
		else
		{
		std:string test_nip = std::to_string(nip_n);
			if (test_nip.length() > 10)
			{
				cout << "Podany Nr NIP ma wiecej niz 10 znakow!. Podaj numer nip jeszcze raz:\n";
				std::cin.clear();
				std::cin.ignore(1000, '\n');
				continue;
			}
			else if (test_nip.length() < 10)
			{
				cout << "Podany Nr NIP ma mniej niz 10 znakow!. Podaj numer nip jeszcze raz:\n";
				std::cin.clear();
				std::cin.ignore(1000, '\n');
				continue;
			}
			else break;
		}
	}
}
void NABYWCA_DOSTAWCA::getNIP_Odbiorca()
{
	cout << "Podaj NIP firmy odbierajacej (10 cyfr)" << endl;
	while (1)
	{
		cin >> nip_o;
		if (cin.good()) break;
		if (cin.fail())
		{
			cout << "Podany Nr NIP nie jest liczba! Prosze wprowadzic Nr NIP jeszcze raz:\n";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
	}
}
void NABYWCA_DOSTAWCA::getMiasto_n()
{
	cout << "Podaj miasto nabywcy: " << endl;
	while (1)
	{
		cin.ignore();
		getline(cin, miasto_n);
		transform(miasto_n.begin(), miasto_n.end(), miasto_n.begin(), ::toupper);
		if ((miasto_n.at(0) >= '0' && miasto_n.at(0) <= '9') || (miasto_n.at(1) >= '0' && miasto_n.at(1) <= '9'))
		{
			cout << "Blad nie podano znaku! Wprowadz miasto jeszcze raz.\n";
			std::cin.clear();
			std::cin.ignore();
		}
		else break;
	}
}
void NABYWCA_DOSTAWCA::getMiasto_o()
{
	cout << "Podaj miasto odbiorcy: " << endl;
	while (1)
	{
		cin.ignore();
		getline(cin, miasto_o);
		transform(miasto_o.begin(), miasto_o.end(), miasto_o.begin(), ::toupper);
		if ((miasto_o.at(0) >= '0' && miasto_o.at(0) <= '9') || (miasto_o.at(1) >= '0' && miasto_o.at(1) <= '9'))
		{
			cout << "Blad nie podano znaku! Wprowadz miasto jeszcze raz.\n";
			std::cin.clear();
			std::cin.ignore();
		}
		else break;
	}
}
string NABYWCA_DOSTAWCA::getKod_AB()
{
	string COM_AB = "COM+AB:FIRMA ";
	string VA = "VA:";
	string MT = "MT:";
	string KO = "KO:";
	std::string NIP_N = std::to_string(nip_n);
	kod_AB = COM_AB + nabywca + '+' + VA + NIP_N + '+' + MT + miasto_n + '+' + KO + kod_pocztowy_n + "'" + '\n';
	cout << kod_AB;
	return kod_AB;
}
string NABYWCA_DOSTAWCA::getKod_BB()
{
	string COM_BB = "COM+BB:FIRMA ";
	string VA = "VA:";
	string MT = "MT:";
	string KO = "KO:";
	std::string NIP_O = std::to_string(nip_o);
	kod_BB = COM_BB + odbiorca + '+' + VA + NIP_O + '+' + MT + miasto_o + '+' + KO + kod_pocztowy_o + "'" + '\n';
	cout << kod_BB;
	return kod_BB;
}
