#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string>
using namespace std;
#pragma once
class NABYWCA_DOSTAWCA
{
private:
	string nabywca;
	string odbiorca;
	long long nip_n;
	long long nip_o;
	string Nip_N;
	string Nip_O;
	string miasto_n;
	string miasto_o;
	string kod_pocztowy_n;
	string kod_pocztowy_o;
	string kod_AB;
	string kod_BB;

public:
	NABYWCA_DOSTAWCA();
	void setNabywca(string nabywca);
	void setOdbiorca(string odbiorca);
	void setKod_pocztowy_n(string kod_pocztowy_n);
	void setKod_pocztowy_o(string kod_pocztowy_o);
	void setNIP_Nabywca(long long nip_n);
	void setNIP_Odbiorca(long long nip_o);
	void setMiasto_n(string miasto_n);
	void setMiasto_o(string miasto_o);
	void setKod_AB(string kod_AB);
	void setKod_BB(string kod_BB);

	void getNabywca();
	void getOdbiorca();
	void getKod_pocztowy_n();
	void getKod_pocztowy_o();
	void getNIP_Nabywca();
	void getNIP_Odbiorca();
	void getMiasto_n();
	void getMiasto_o();
	string getKod_AB();
	string getKod_BB();



};

