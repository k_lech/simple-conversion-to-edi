#include "stdafx.h"
#include "POCZATEK_KOMUNIKATU.h"
#include <algorithm>

POCZATEK_KOMUNIKATU::POCZATEK_KOMUNIKATU()
	{
	}
	void POCZATEK_KOMUNIKATU::setWartosc(string wartosc)
	{
		this->wartosc = wartosc;
	}
	void POCZATEK_KOMUNIKATU::setKod(int kod)
	{
		this->kod = kod;
	}
	void POCZATEK_KOMUNIKATU::getWartosc()
	{
		cout << "Podaj typ dokumentu \n1.Faktura / 2.Paragon" << endl;
		string wartosc;

		while (1)
		{
			cin >> wartosc;
			wartosc.erase(remove_if(wartosc.begin(), wartosc.end(), isspace), wartosc.end());
			transform(wartosc.begin(), wartosc.end(), wartosc.begin(), ::tolower);

			if (wartosc == "faktura")
			{
				this->setKod(380);
				break;
			}
			else if (wartosc == "paragon")
			{
				this->setKod(372);
				break;
			}
			else if (wartosc != "faktura")
			{
				cout << "Podano nieprawidlowy typ dokumentu! Prosze wprowadzic typ dokumentu jeszcze raz:\n";
				std::cin.clear();
				std::cin.ignore(1000, '\n');
			}
			else if (wartosc != "paragon")
			{
				cout << "Podano nieprawidlowy typ dokumentu! Prosze wprowadzic typ dokumentu jeszcze raz:\n";
				std::cin.clear();
				std::cin.ignore(1000, '\n');
			}
		}
	}
	void POCZATEK_KOMUNIKATU::setRef(int ref)
	{
		this->ref = ref;
	}
	void POCZATEK_KOMUNIKATU::getRef()
	{
		cout << "Podaj Nr Referencyjny (max 9 cyfr)" << endl;
		while (1)
		{
			cin >> ref;
			if (cin.good()) break;
			if (cin.fail())
			{
				cout << "Podany Nr Referencyjny nie jest liczba! Prosze wprowadzic Nr Referencyjny jeszcze raz:\n";
				std::cin.clear();
				std::cin.ignore(1000, '\n');
				continue;
			}
		}
	}
	void POCZATEK_KOMUNIKATU::setWartosc_1(string wartosc_1)
	{
		this-> wartosc_1 = wartosc_1;
	}
	void POCZATEK_KOMUNIKATU::setWersja(int wersja)
	{
		this->wersja = wersja;
	}
	void POCZATEK_KOMUNIKATU::getWersja()			//Pobieranie wersji dokumentu	
	{
		cout << "Podaj wersje dokumentu\nDostepne opcje: 1. Duplikat / 2.Oryginal / 3.Kopia\n";
		while (1)
		{
			cin >> wartosc_1;
			wartosc_1.erase(remove_if(wartosc_1.begin(), wartosc_1.end(), isspace), wartosc_1.end());
			transform(wartosc_1.begin(), wartosc_1.end(), wartosc_1.begin(), ::tolower);
			if (wartosc_1 == "duplikat")
			{
				wersja = 7;
				break;
			}
			else if (wartosc_1 == "oryginal")
			{
				wersja = 9;
				break;
			}
			else if (wartosc_1 == "kopia")
			{
				wersja = 31;
				break;
			}
			else if (wartosc_1 != "duplikat")
			{
				cout << "Podano nieprawidlowa wersje dokumentu!\n";
				std::cin.clear();
				std::cin.ignore(1000, '\n');
				continue;
			}
			else if (wartosc_1 != "orygina�")
			{
				cout << "Podano nieprawidlowa wersje dokumentu!\n";
				std::cin.clear();
				std::cin.ignore(1000, '\n');
				continue;
			}
			else if (wartosc_1 != "kopia")
			{
				cout << "Podano nieprawidlowa wersje dokumentu!\n";
				std::cin.clear();
				std::cin.ignore(1000, '\n');
				continue;
			}
		}
		cout << endl;
	}
	void POCZATEK_KOMUNIKATU::setKomunikat(string caly_kod)
	{
		this->caly_kod = caly_kod;
	}
	string POCZATEK_KOMUNIKATU::getKomunikat()
	{
		std::string kod_dokumentu = std::to_string(kod);
		std::string nr_referenyjny = std::to_string(ref);
		std::string wersja_dokumentu = std::to_string(wersja);
		string BGM = "BGM";
		string UNA = "UNA'\n";
		caly_kod = UNA + BGM + '+' + kod_dokumentu + '+' + nr_referenyjny + '+' + wersja_dokumentu + "'" + '\n';
		cout << caly_kod;
		return caly_kod;
	}
