#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string>
using namespace std;
#pragma once
class PLATNOSCI
{
private:
	string kod_pay;
	string wartosc_2;
	int sposob_platnosci;
	int kwota;
	string waluta;

public:
	PLATNOSCI();

	void getWartosc_2();
	string getKod_pay();
	void getKwota();
	void getWaluta();

	
	void setWartosc_2(string wartosc_2);
	void setSposob_platnosci(int sposob_platnosci);
	void setKwota(int kwota);
	void setWaluta(string waluta);

	void setKod_pay(string kod_pay);



};

