/*Program do zapisu pliku (.txt) skladajacego sie z układu TAGÓW, tj:
POCZATEK KOMUNIKATU							(Faktura handlowa / Paragon; Nr Referenyjny (o dowolnej długości); Wersja dokumentu: duplikat / oryginał / kopia)
PLATNOSCI									(Sposób płatności: gotówka / karta kredytowa / przelew krajowy / przelew zagraniczny; Kwota; Waluta)
IDENTYFIKATOR CZASU/DATY					(Data: wystawienia faktury / płatnosci / dostawy; Data w formacie wybranym przez użytkownika (RRRRMMDD / DDMMRRRR)
IDENTYFIKATOR NABYWCY I DOSTAWCY			(Nazwa nabywcy / adres / nip / miasto / kod pocztowy)
LISTA POZYCJI TOWAROWYCH (do 6 pozycji)		(Nr pozycji / Nazwa towaru / ilość / cena jednostkowa / cena całkowita)
OSOBA WYSTAWIAJACA DOKUMENT					(Imie i nazwisko)


Program wykonał: Krystian Lech
*/
#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <fstream>
#include <cstdlib>
#include "POCZATEK_KOMUNIKATU.h"
#include "IDENTYFIKACJA_DATY.h"
#include "PLATNOSCI.h"
#include "POZYCJE_TOWAROWE.h"
#include "NABYWCA_DOSTAWCA.h"
#include "WYSTAWIAJACY.h"
#include "LICZBA_POZ_TOW.h"
using namespace std;
void funkcje_programu() 
{
	cout << "Program do kodowania informacji. W programie sa nastepujace funkcje: \n"
		<< " [1] Poczatek komunikatu (kod dokumentu / nr referenycjny / wersja dokumentu) \n"
		<< " [2] Identyfikator czasu i daty (data wystawienia dokumentu / format zapisu daty) \n"
		<< " [3] Identyfikator nabywcy i dostawcy (Nazwa / NIP / Miasto / kod pocztowy) \n"
		<< " [4] Pozycje towarowe (nazwa towaru / ilosc / cena jednostkowa / cena razem) \n"
		<< " [5] Dane osoby wprowadzajacej komunikat (imie i nazwisko) \n"
		<< " [6] Platnosc (sposob platnosci / kwota / waluta) \n"
		<< " [7] Wyswietlanie komunikatu \n"
		<< " [8] Zapis komunikatu \n"
		<< " [9] Wyjscie z programu \n"
		<< "UWAGA!!!\n"
		<< "W celu uzyskania kompletnego raportu, nalezy wykonac wszystkie kroki od 1-6, w dowolnej kolejnosci \ni dopiero wtedy dokonac zapisu, poprzez funkcje nr 8! \n\n";
}


int main()
{
	int wybor;
	funkcje_programu();
	POCZATEK_KOMUNIKATU nowy_komunikat;		//Deklaracje:
	IDENTYFIKACJA_DATY nowa_data;
	NABYWCA_DOSTAWCA nowy_klient;
	POZYCJE_TOWAROWE nowy_towar;
	WYSTAWIAJACY nowy_wystawiajacy;
	PLATNOSCI nowa_platnosc;
	LICZBA_POZ_TOW nowa_lista;
	do
	{
		cout << "Wybierz modul programu: \n";
		cin >> wybor;
		cout << endl;
		switch (wybor)
		{
		case 1:
		{
			cout << "   [1] Poczatek komunikatu: \n";
			nowy_komunikat.getWartosc();			//Typ dokumentu
			nowy_komunikat.getRef();				//Numer referenycyjny
			nowy_komunikat.getWersja();				//Wersja dokumentu
		}
			break;
		case 2:
		{
			cout << "   [2] Identyfikator czasu i daty: \n";
			nowa_data.getKod();
			nowa_data.getData();
			nowa_data.getFormat();
		}
			break;
		case 3:
		{
			cout << "   [3] Identyfikator nabywcy i dostawcy: \n";
			nowy_klient.getNabywca();			//Nabywca
			nowy_klient.getNIP_Nabywca();
			nowy_klient.getMiasto_n();
			nowy_klient.getKod_pocztowy_n();

			nowy_klient.getOdbiorca();			//Odbiorca
			nowy_klient.getNIP_Odbiorca();
			nowy_klient.getMiasto_o();
			nowy_klient.getKod_pocztowy_o();
		}
			break;
		case 4:
		{
			cout << "   [4] Pozycje towarowe: \n";
			nowy_towar.getWyczysc_KOD_Pozycji_towarowej();	
			nowa_lista.getVoidLiczba_pozycji();
			for (int i = 1; i <= nowa_lista.getLiczba_pozycji(); i++)
			{
				nowy_towar.getNr_pozycji();
				nowy_towar.getNazwa_towaru();
				nowy_towar.getIlosc();
				nowy_towar.getCena_jednostkowa();
				nowy_towar.getCena();
				nowy_towar.getPozycjaTowarowa();
				nowy_towar.getKOD_Pozycji_towarowej();
			}
			nowy_towar.getKOD_Pozycji_towarowej();		
		}
			break;
		case 5:
		{
			cout << "   [5] Osoba wystawiajaca dane: \n";	
			nowy_wystawiajacy.getOsoba();
		}
			break;
		case 6:
		{
			cout << "   [6] Platnosci: \n";
			nowa_platnosc.getWartosc_2();			//Sposób płatności
			nowa_platnosc.getKwota();				//Kwota
			nowa_platnosc.getWaluta();				//Waluta
		}
			break;

		case 7:
		{
			cout << "   [7] Wyswietlanie danych: \n";
			
			nowy_komunikat.getKomunikat();						//1
			nowa_data.getKOD_DATA();							//2
			nowy_klient.getKod_AB();							//3
			nowy_klient.getKod_BB();							//3
			nowy_towar.getWyswietl_KOD_Pozycji_towarowej();		//4
			nowy_wystawiajacy.getCTA();							//5
			nowa_platnosc.getKod_pay();							//6
			nowa_lista.getKod_wierszy();
		}
			break;
		case 8:
		{
			cout << "   [8] Zapis danych: \n";
			ofstream plik;
			plik.open("RAPORT.txt");
			
			plik << nowy_komunikat.getKomunikat();
			plik << nowa_data.getKOD_DATA();
			plik << nowy_klient.getKod_AB();
			plik << nowy_klient.getKod_BB();
			plik << nowy_towar.getKOD_Pozycji_towarowej();
			plik << nowy_wystawiajacy.getCTA();
			plik << nowa_platnosc.getKod_pay();
			plik << nowa_lista.getKod_wierszy();
			plik.close();
			
		}
		break;
		case 9:
		{
			system("pause");
			return 0;
		}
		default:
			
			break;

		}
	}while (1);
	
	system("pause");
    return 0;
}


