#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string>


using namespace std;

#pragma once
class POCZATEK_KOMUNIKATU
{
private:
	string caly_kod;
	int kod;
	string wartosc;
	int ref;
	string wartosc_1;
	int wersja;
	
public:
	POCZATEK_KOMUNIKATU();

	void getWartosc();
	void getRef();
	void getWersja();
	string getKomunikat();

	void setWartosc(string wartosc);
	void setKod(int kod);

	void setRef(int ref);

	void setWartosc_1(string wartosc_1);
	void setWersja(int wersja);

	void setKomunikat(string caly_kod);
	

};

