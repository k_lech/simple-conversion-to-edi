#include "stdafx.h"
#include "IDENTYFIKACJA_DATY.h"
#include <algorithm>
IDENTYFIKACJA_DATY::IDENTYFIKACJA_DATY()
{
}

void IDENTYFIKACJA_DATY::setKod (int kod)
{
	this->kod = kod;
}
void IDENTYFIKACJA_DATY::getKod()
{
	cout << "Wprowadzona data operacji jest data: 1. Wystawienia faktury / 2. Platnosci / 3. Dostawy" << endl;
	string temp;
	std::cin.ignore();
	while (1)
	{
		getline(cin, temp);
		temp.erase(remove_if(temp.begin(), temp.end(), isspace), temp.end());
		transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
		if (temp == "wystawieniafaktury" || temp == "faktury" || temp == "wystawienia")
		{
			this->setKod(137);
			break;
		}
		else if (temp == "platnosci")
		{
			this->setKod(3);
			break;
		}
		else if (temp == "dostawy")
		{
			this->setKod(4);
			break;
		}
		else if (temp != "wystawienia" || temp != "faktury" || temp != "wystawienia" || temp != "platnosci" || temp != "dostawy")
		{
			cout << "1. Podano nieprawidlowy typ dokumentu! Prosze wprowadzic typ dokumentu jeszcze raz:\n";
			std::cin.clear();
			std::cin.sync();
		}
	}
}
void IDENTYFIKACJA_DATY::setData(int dzien, int miesiac, int rok)
{
	this->dzien = dzien;
	this->miesiac = miesiac;
	this->rok = rok;
}
void IDENTYFIKACJA_DATY::getData()
{
	cout << "Wprowadz date transakcji: Dzien / Miesiac / Rok" << endl;
	cout << "Podaj dzien transackji: " << endl;
	while (1)
	{
		cin >> dzien;
		if (dzien >= 1 && dzien<=31) break;
		else 
		{
			cout << "Blad - Podaj dzien transakcji jeszcze raz:\n";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
	}
	cout << "Podaj miesiac transackji: " << endl;
	while (1)
	{
		cin >> miesiac;
		if (miesiac >= 1 && miesiac <= 12) break;
		else
		{
			cout << "Blad - Podaj miesiac transakcji jeszcze raz:\n";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
	}
	cout << "Podaj rok transackji: " << endl;
	while (1)
	{
		cin >> rok;
		if (rok > 1990 && rok <= 2099) break;
		else
		{
			cout << "Blad - Podaj rok transakcji jeszcze raz:\n";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
	}
}
void IDENTYFIKACJA_DATY::setFormat(string data, int format)
{
	this->data = data;
	this->format = format;
}
void IDENTYFIKACJA_DATY::getFormat()
{
	cout << "Wybierz format wprowadzania daty:    1. RRRR-MM-DD    /   2. DD-MM-RRRR"
		"\n* Wprowadz numer odpowiadajacej opcji" << endl;
	std::string ROK = std::to_string(rok);
	std::string MIESIAC = std::to_string(miesiac);
	std::string DZIEN = std::to_string(dzien);
	while (1)
	{
		cin >> format;
		if (format == 1 || format == 2) break;
		else
		{
			cout << "Blad, nie wprowadzono odpowiedniej komeny, wprowadz \n1. aby zapisac date w formacie RRRR-MM-DD\n 2. aby zapisac date w formacie  DD-MM-RRRR\n";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
	}
	if (DZIEN.length() <= 1)
	{
		DZIEN = "0" + DZIEN;
	}
	if (MIESIAC.length() <= 1)
	{
		MIESIAC = "0" + MIESIAC;
	}
	if (format == 1)
	{
		data = ROK + MIESIAC + DZIEN;
		format = 102;
	}
	else if (format == 2)
	{
		data = DZIEN + MIESIAC + ROK;
		format = 103;
	}
	cout << endl;
}
void IDENTYFIKACJA_DATY::setKOD_DATA(string kod_DATA)
{
	this->kod_DATA = kod_DATA;
}
string IDENTYFIKACJA_DATY::getKOD_DATA()
{
	std::string KOD = std::to_string(kod);
	std::string FORMAT = std::to_string(format);	// konwersja z int do string
	string DTM = "DTM+";
	kod_DATA = DTM + KOD + '+' + data + '+' + FORMAT + '\n';
	cout << kod_DATA;
	return kod_DATA;
}


