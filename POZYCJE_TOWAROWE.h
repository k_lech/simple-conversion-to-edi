#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string>
using namespace std;
#pragma once
class POZYCJE_TOWAROWE
{
private:
	
	int nr_pozycji;
	string nazwa_towaru;
	int ilosc;
	double cena_jednostkowa;
	double cena;
	string pozycja_towarowa;
	string KOD_Pozycji_towarowej;

public:
	POZYCJE_TOWAROWE();
	
	void setNr_pozycji (int nr_pozycji);
	void setNazwa_towaru (string nazwa_towaru);
	void setIlosc (int ilosc);
	void setCena_jednostkowa (double cena_jednostkowa);
	void setCena (double cena);
	void setPozycjaTowarowa (string pozycja_towarowa);
	void setKOD_Pozycji_towarowej(string KOD_Pozycji_towarowej);
	void setWyczysc_KOD_Pozycji_towarowej(string KOD_Pozycji_towarowej);

	void getNr_pozycji();
	void getNazwa_towaru();
	void getIlosc();
	void getCena_jednostkowa();
	void getCena();
	void getPozycjaTowarowa();
	void getWyczysc_KOD_Pozycji_towarowej();
	void getWyswietl_KOD_Pozycji_towarowej();
	string getKOD_Pozycji_towarowej();
};