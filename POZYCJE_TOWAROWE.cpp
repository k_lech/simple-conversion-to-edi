#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include "POZYCJE_TOWAROWE.h"


POZYCJE_TOWAROWE::POZYCJE_TOWAROWE()
{
}


void POZYCJE_TOWAROWE::setNr_pozycji(int nr_pozycji)
{
	this->nr_pozycji = nr_pozycji;
}
void POZYCJE_TOWAROWE::setNazwa_towaru(string nazwa_towaru)
{
	this->nazwa_towaru = nazwa_towaru;
}
void POZYCJE_TOWAROWE::setIlosc(int ilosc)
{
	this->ilosc = ilosc;
}
void POZYCJE_TOWAROWE::setCena_jednostkowa(double cena_jednostkowa)
{
	this->cena_jednostkowa = cena_jednostkowa;
}
void POZYCJE_TOWAROWE::setCena(double cena)
{
	this->cena = cena;
}
void POZYCJE_TOWAROWE::setPozycjaTowarowa(string pozycja_towarowa)
{
	this->pozycja_towarowa = pozycja_towarowa;
}
void POZYCJE_TOWAROWE::setKOD_Pozycji_towarowej(string KOD_Pozycji_towarowej)
{
	this->KOD_Pozycji_towarowej = KOD_Pozycji_towarowej;
}
void POZYCJE_TOWAROWE::setWyczysc_KOD_Pozycji_towarowej(string KOD_Pozycji_towarowej)
{
	this->KOD_Pozycji_towarowej = KOD_Pozycji_towarowej;
}

void POZYCJE_TOWAROWE::getNr_pozycji()
{
	cout << "Podaj numer pozycji towarowej:";	
	while (1)
	{
		cin >> nr_pozycji;
		if (nr_pozycji<6) break;
		else if (nr_pozycji < 6)
		{
			cout << "Nieobslugiwana ilosc pozycji towarowych (>6). Wprowadz jeszcze raz:" << endl;
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
	}
}
void POZYCJE_TOWAROWE::getNazwa_towaru()
{
	cout << "Podaj nazwe towaru: " << endl;
	std::cin.ignore();
	getline(cin, nazwa_towaru);
	transform(nazwa_towaru.begin(), nazwa_towaru.end(), nazwa_towaru.begin(), ::toupper);
}
void POZYCJE_TOWAROWE::getIlosc()
{
	cout << "Podaj ilosc zakupionego towaru: " << nazwa_towaru << endl;
	while (1)
	{
		cin >> ilosc;
		if (cin.good()) break;
		if (cin.fail())
		{
			cout << "Wprowadzono nienieporawna wartosc, prosze wprowadzic liczbe:\n";
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			continue;
		}
	}
}
void POZYCJE_TOWAROWE::getCena_jednostkowa()
{
	cout << "Podaj cene jednostkowa, towaru: " << nazwa_towaru << endl;
	while (1)
	{
	cin >> cena_jednostkowa;
	if (cin.good())
	{
		break;
	}
	if (cin.fail())
	{
		cout << "Wprowadzono nienieporawna cene, prosze wprowadzic cyfry:\n";
		std::cin.clear();
		std::cin.ignore(1000, '\n');
		continue;
	}
}
}
void POZYCJE_TOWAROWE::getCena()
{
	cena = cena_jednostkowa * ilosc;
}
void POZYCJE_TOWAROWE::getPozycjaTowarowa()
{
	string LIN = "LIN+";
	string IM = "IM:";
	string NU = "NU:";
	string PR = "PR:";
	string RPA = "RPA:";
	std::string NR_POZYCJI = std::to_string(nr_pozycji);
	std::string ILOSC = std::to_string(ilosc);
	stringstream stream;
	stream << fixed << setprecision(2) << cena_jednostkowa;
	string CENA_JEDNOSTKOWA = stream.str();
	stringstream stream2;
	stream2 << fixed << setprecision(2) << cena;
	string CENA = stream2.str();
	pozycja_towarowa += LIN + NR_POZYCJI + '+' + IM + nazwa_towaru + '+' + NU + ILOSC + '+' + PR + CENA_JEDNOSTKOWA + '+' + RPA + CENA + "'" + "\n";
}
void POZYCJE_TOWAROWE::getWyswietl_KOD_Pozycji_towarowej()
{
	cout << KOD_Pozycji_towarowej;
}
void POZYCJE_TOWAROWE::getWyczysc_KOD_Pozycji_towarowej()
{
	KOD_Pozycji_towarowej = 'N';
}
string POZYCJE_TOWAROWE::getKOD_Pozycji_towarowej()
{
	KOD_Pozycji_towarowej = pozycja_towarowa;
	return KOD_Pozycji_towarowej;
}